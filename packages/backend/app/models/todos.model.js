
module.exports = (sequelize, Sequelize) => {
  const ToDo = sequelize.define("todolists", {
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
    },
    todo: {
      type: Sequelize.STRING
    },
    isCompleted: {
      type: Sequelize.BOOLEAN,
    },
    createdAt: {
      type: Sequelize.DATE
    }
  });

  return ToDo;
};
