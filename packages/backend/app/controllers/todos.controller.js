const db = require("../models");
const ToDo = db.todos;
const Op = db.Sequelize.Op;

// Create and Save a new ToDo
exports.create = (req, res) => {
    // Validate request
    if (!req.body.todo) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a Tutorial
    const todo = {
      todo: req.body.todo,
      isCompleted: req.body.isCompleted ? req.body.isCompleted : false
    };
  
    // Save Tutorial in the database
    ToDo.create(todo)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the ToDoList."
        });
      });
  };

  exports.findAll = (req, res) => {

    ToDo.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
  };

  exports.update = (req, res) => {
    const id = req.params.id;
  
    ToDo.update(req.body, {
      where: { id: id }
    })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "ToDo was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update ToDo with id=${id}. Maybe ToDo was not found or req.body is empty!`
        });
      }
    })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Todo with id=" + id
        });
      });
  };
  exports.delete = (req, res) => {
    const id = req.params.id;
    ToDo.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ToDO was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete ToDO with id=${id}. Maybe ToDO was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete ToDO with id=" + id
        });
      });
  };